![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)

# TEI from eScriptorium

This is a simple python project enabling eScriptorium users to download TEI files from the application. It uses [`escriptorium-connector`](https://gitlab.com/sofer_mahir/escriptorium_python_connector) and XSLT to download PAGE XML files and transform them into TEI XML.


## Installation

Create an environment and install requirements.

Do not install `escriptorium-connector` via pip: we updated some of its behavior.

## Usage
To start the scrip, activate the environment and run:  
- `python3 tei_from_escriptorium.py [-i]`

The interactive option only applies to the connection information, otherwise the script will always need you to pass information to choose which documents and which pages (called document parts) will be download and transformed into TEI.


## Connection information

Use `config.py` to add your login, password and url of the eScriptorium app. 

If you wish, you can also use the interactive mode (`-i`, or `--interactive`) when launching the python script.

A third option is to use dot env. As explained in the original [`escriptorium-connector`](https://gitlab.com/sofer_mahir/escriptorium_python_connector) package documentation:  

> If you are working on a public repository, you will probably want to store your user credentials in a hidden `.env` file that does not get distributed with your code. This is pretty easy to accomplish with [python-dotenv](https://pypi.org/project/python-dotenv/). You will need to provide the connector with an eScriptorium instance URL, the API URL, your username, and your password (see below).
> 
> The `EscriptoriumConnector` class provides (or will provide) all the methods needed to interact programmatically with the eScriptorium platform.
> 
> Example usage:
> 
> ```python
> From escriptorium_connector import EscriptoriumConnector
> import os
> from dotenv import load_dotenv
> 
> 
> if __name__ == '__main__':
>     load_dotenv()
>     url = str(os.getenv('ESCRIPTORIUM_URL'))
>     api = f'{url}api/'
>     username = str(os.getenv('ESCRIPTORIUM_USERNAME'))
>    password = str(os.getenv('ESCRIPTORIUM_PASSWORD'))
>     escr = EscriptoriumConnector(url, api, username, password)
>     print(escr.get_documents())
> 
> ```
> 
> And your `.env` file should have:
> 
> ```txt
> ESCRIPTORIUM_URL=https://www.escriptorium.fr/
> ESCRIPTORIUM_USERNAME=your escriptorium username
> ESCRIPTORIUM_PASSWORD=your escriptorium password
> ```


If you wish to activate this option, you have to change the script L246 to 254:

```
else:
    #from config import USERNAME, PASSWORD, URL
    # Or if dotenv instead of config
    import dotenv
    load_dotenv()
    URL = str(os.getenv('ESCRIPTORIUM_URL'))
    USERNAME = str(os.getenv('ESCRIPTORIUM_USERNAME'))
    PASSWORD = str(os.getenv('ESCRIPTORIUM_PASSWORD'))
```
