# -*- coding: utf-8 -*-

"""Simple CLI for downloading PAGE XML files from eScriptorium
and transforming them into TEI XML using XSLT
"""

import argparse
from datetime import datetime as dt
import getpass
import os
import subprocess
from zipfile import ZipFile

import pprint
import requests
from tqdm import tqdm
import urllib3


# exec from esc_connector/
from esc_py_conn.escriptorium_connector import EscriptoriumConnector

# setting prettyprint
pp = pprint.PrettyPrinter(indent=4)
# disable InsecureRequestWarning
urllib3.disable_warnings()


# =========== FUNCs =========================
def pywget(url, target_name=None, verify=True):
    r = requests.get(url, allow_redirects=True, verify=verify)
    if r.status_code != 200:
        r.raise_for_status()
    else:
        if not target_name:
            target_name = os.path.basename(url)
        open(target_name, "wb").write(r.content)


def get_saxon(path_to_files):
    url_saxon = "https://repo1.maven.org/maven2/net/sf/saxon/Saxon-HE/9.9.1-7/Saxon-HE-9.9.1-7.jar"
    path_to_saxon = os.path.join(path_to_files, os.path.basename(url_saxon))
    pywget(url_saxon, path_to_saxon)
    return path_to_saxon


def get_xslt(path_to_files):
    url_xsl = "https://raw.githubusercontent.com/lectaurep/page2tei/main/xmlpage_to_tei.xsl"
    path_to_xsl = os.path.join(path_to_files, os.path.basename(url_xsl))
    pywget(url_xsl, path_to_xsl)
    return path_to_xsl


def control_creds_validity(url, username, password):
    if len(str(url)) == 0 or len(str(username)) == 0 or len(str(password)) == 0:
        print("Missing eScriptorium app URL, or login or password.")
    elif not str(url).startswith("http"):
        print("The URL of the eScriptorium app seems invalid.")
    else:
        return True


def ask_for_num(noptions, category):
    print(f"List of {category} options:")
    for num, name in noptions:
        print(f"[{num}] '{name}'")
    if len(noptions) == 1:
        selected_option = noptions[0][1]
        print(f"Only 1 option, automatically choosing '{selected_option}'")
    else:
        authorized = [n for n, opt in noptions]
        choice = int(input(f"Please, use number to choose {category} -> "))
        while choice not in authorized:
            choice = input(f"Incorrect value, please, use number to choose {category} -> ")
            if choice.isdigit():
                choice = int(choice)
            else:
                choice = -1
        selected_option = noptions[choice][1]
        print(f"You chose '{selected_option}'")
    print("- - - -")
    return selected_option


def choose_project(docs):
    projects = set([elem.get("project", None) for elem in docs if elem.get("project", None)])
    counted_projects = [(n, pn) for n, pn in enumerate(projects)]
    project_name = ask_for_num(counted_projects, "project")
    return project_name


def get_selection(list_of_items, category):
    candidates = [", pk: ".join([str(item["name"]), str(item["pk"])]) for item in list_of_items]
    counted_candidates = [(n, cn) for n, cn in enumerate(candidates)]
    selected = ask_for_num(counted_candidates, category)
    return selected.split(", pk: ")


def choose_document(docs, project):
    valid_docs = [elem for elem in docs if elem.get("project", None) == project]
    return get_selection(valid_docs, "document")


def choose_transcription_version(doc_info):
    transcriptions = doc_info.get("transcriptions", [])
    return get_selection(transcriptions, "transcription")


def check_regtype_selection(choice, authorized_range):
    if len(choice) == 0:
        print("Chosing all region types available.")
        return authorized_range
    invalid = False
    split_choice = [elem.strip() for elem in choice.split(",")]
    selection = []
    for elem in split_choice:
        if not elem.isdigit():
            invalid = True
        else:
            selection.append(int(elem))
    if invalid:
        print("You must use integers and separate multiple selection with ','")
        return False
    else:
        kept = [choice for choice in selection if choice in authorized_range]
        if len(kept) < len(selection):
            print("You must choose values within the available options!")
            return False
        else:
            return kept


def choose_region_type(doc_info):
    valid_region_types = doc_info.get("valid_block_types", [])
    candidate_region_types = [", pk: ".join([str(item["name"]), str(item["pk"])]) for item in valid_region_types]
    candidate_region_types += ["Undefined region type, pk: Undefined", 
                                "Orphan lines, pk: Orphan"]
    counted_region_types = [(n, cn) for n, cn in enumerate(candidate_region_types)]
    offset = len(counted_region_types)

    authorized_values = [n for n in range(len(counted_region_types))]
    print("\n".join(["Choose region types to export",
                     "**help**",
                     "- use ',' to select multiple option (ex: 1, 4, 5)",
                     "- leave empty to select all images"]))
    print(f"List of options:")
    for num, name in counted_region_types:
        print(f"[{num}] '{name}'")
    choice = input(f"Your choice -> ")
    selected = check_regtype_selection(choice, authorized_values)
    while selected is False:
        choice = input(f"Available options: [{', '.join([str(i) for i in authorized_values])}] -> ")
        selected = check_regtype_selection(choice, authorized_values)
    reg_parts_pks = [elem[1].split("pk:")[-1].strip() for elem in counted_region_types if elem[0] in selected]
    print("- - - -")
    return reg_parts_pks



def check_docpart_selection(choice, authorized_range):
    if len(choice) == 0:
        print("Chosing all document parts available.")
        return authorized_range
    invalid = False
    split_choice = [elem.strip() for elem in choice.split(",")]
    selection = []
    for elem in split_choice:
        if '-' in elem:
            outliers = [ol.strip() for ol in elem.split('-')]
            for n, ol in enumerate(outliers):
                if not ol.isdigit():
                    invalid = True
                    outliers.pop(n)
            if len(outliers) == 2:
                for i in range(int(outliers[0]), int(outliers[1]) + 1):
                    selection.append(i)
            else:
                invalid = True
        elif not elem.isdigit():
            invalid = True
        else:
            selection.append(int(elem))
    if invalid:
        print("You must use integers and separate your selection with '-' or ','")
        return False
    else:
        kept = [choice for choice in selection if choice in authorized_range]
        if len(kept) < len(selection):
            print("You must choose values within the indicated range!")
            return False
        else:
            return kept


def choose_doc_parts(doc_parts):
    candidate_doc_parts = [(int(elem["order"])+1, elem["order"], elem["pk"]) for elem in doc_parts]
    authorized_values = [elem[0] for elem in candidate_doc_parts]
    authorized_range = f"{min(authorized_values)} - {max(authorized_values)}"
    print("\n".join(["Choose document parts to export",
                     "**help**",
                     "- use '1, 4, 5' to select individual parts",
                     "- use '1-3' to select a range of parts",
                     "- individual selection and range can be combined : '1-5, 7'",
                     "- leave empty to select all images"]))
    choice = input(f"Available range: {authorized_range} -> ")
    selected = check_docpart_selection(choice, authorized_values)
    while selected is False:
        choice = input(f"Available range: {authorized_range} -> ")
        selected = check_docpart_selection(choice, authorized_values)
    show = input("Do you want to control which document parts will be downloaded? (y)")
    if show .lower() == 'y':
        print(selected)
    # let's get the pks
    doc_parts_pks = [elem[2] for elem in candidate_doc_parts if elem[0] in selected]
    return doc_parts_pks


def unzip_file(path_to_zip, path_to_dir):
    zph = ZipFile(os.path.join(".", path_to_zip), 'r')
    files = zph.infolist()
    for file in files:
        zph.extract(file, path=path_to_dir)
    zph.close()
    return


def main(url, username, password):
    path_to_files = os.path.join(os.path.dirname(os.path.abspath(__file__)), "files")
    path_to_saxon = get_saxon(path_to_files)
    path_to_xsl = get_xslt(path_to_files)

    # 1. Download files from eScriptorium
    if not url.endswith('/'):
        url += "/"
    api = f'{url}api/'
    # opening session
    try:
        escr = EscriptoriumConnector(url, api, username, password)
    except Exception:
        print("Something went wrong while connecting to the server: make sure your credentials are correct!")
        return False

    today = dt.now().strftime("%Y_%m_%d")

    # getting all the info necessary to download files
    list_of_docs = escr.get_documents()
    project_name = choose_project(list_of_docs)
    doc_name, doc_pk = choose_document(list_of_docs, project_name)
    doc_info = [elem for elem in list_of_docs if int(elem.get("pk", -1)) == int(doc_pk)][0]
    tran_name, tran_pk = choose_transcription_version(doc_info)
    region_types = choose_region_type(doc_info)
    available_doc_parts = escr.get_document_parts(doc_pk)
    doc_parts_pks = choose_doc_parts(available_doc_parts)

    # downloading files (they come as a zip archive)
    print("Starting downloading files from eScriptorium")
    zip_file_name = os.path.join(path_to_files, f"export_{doc_pk}_{tran_pk}_{today}.zip")
    dir_name = zip_file_name[:-4]
    zip_as_bytes = escr.download_part_pagexml_transcription(doc_pk, doc_parts_pks, tran_pk, region_types)
    with open(zip_file_name, "wb") as fh:
        fh.write(zip_as_bytes)
    unzip_file(zip_file_name, dir_name)
    # os.remove(zip_file_name)  # optional

    # building java env
    env = dict(os.environ)
    env["JAVA_OPTS"] = "foo"

    # transforming files into TEI XML with saxon and xslt
    print("Starting transformation to TEI")
    for page_xml in tqdm(os.listdir(dir_name)):
        if not page_xml.endswith(".xml"):
            continue
        path_to_pagexml = os.path.join(dir_name, page_xml)
        path_to_teixml = path_to_pagexml.replace(".xml", ".tei.xml")
        try:
            # !java -jar $path_to_saxon -xsl:$path_to_xsl -s:$path_to_pagexml -o:$path_to_teixml
            subprocess.run(["java", "-jar", path_to_saxon, f"-xsl:{path_to_xsl}",
                            f"-s:{path_to_pagexml}", f"-o:{path_to_teixml}"], env=env)
        except Exception as e:
            print(f"Error on {path_to_pagexml}")
            pp.pprint(e)
    print("Finished!")


# ====================================================================================================================
parser = argparse.ArgumentParser(description="Download TEI XML from eScriptorium")
parser.add_argument("-i", "--interactive", action="store_true",
                    help="Toggle interactive mode (default behavior is: load info from config)")

args = parser.parse_args()

if args.interactive:
    URL = input("What is the URL of the eScriptorium application you wish to connect to? -> ")
    USERNAME = input("What is your username? -> ")
    PASSWORD = getpass.getpass("What is your password? -> ")
else:
    from config import USERNAME, PASSWORD, URL
    """
    # Or if dotenv instead of config
    import dotenv
    load_dotenv()
    URL = str(os.getenv('ESCRIPTORIUM_URL'))
    USERNAME = str(os.getenv('ESCRIPTORIUM_USERNAME'))
    PASSWORD = str(os.getenv('ESCRIPTORIUM_PASSWORD'))
    """

if control_creds_validity(URL, USERNAME, PASSWORD):
    main(URL, USERNAME, PASSWORD)
else:
    print("Failed to start process: check your config.py file or make sure to activate interactive mode (-i)")



